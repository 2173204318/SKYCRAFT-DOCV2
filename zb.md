# 正版验证

认证完成后,您将会获得:

* 服务器内正版玩家专属\[<mark style="color:green;">**绿头√]**</mark>称号(需要使用服务器材质)

<mark style="color:purple;">**适用于Java版的正版验证步骤:(支持XGP等所有Java版正版渠道)**</mark>\
1.新建服务器 - 加入服务器(1.7-1.18客户端均可) <mark style="color:green;">IP: zb.skycraft.cn</mark>\
2.当客户端显示 验证成功 即完成正版验证\
3.返回登录服,在记分板查看称号是否正常佩戴.

<mark style="color:purple;">****</mark>

<mark style="color:red;">**由于基岩版的可破解性,服务器暂不提供基岩版(Win10)/基岩版(Google Play)/基岩版(App Store)的自动化与手动化的正版验证服务.**</mark>
