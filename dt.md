# 顶服务器宣传贴奖励(奖励天空点)

{% hint style="info" %}
请勿使用小号顶帖,发现将会受到严惩!(MCBBS版规要求)
{% endhint %}

### <mark style="color:purple;">**在MCBBS对服务器宣传贴使用顶贴卡,即可获得高昂奖励**</mark>

### 服务器长期额外奖励:[天空点](sky.md),游戏币

### 顶帖奖励

▲当周五至周日顶贴时,会发放2天空点与500游戏币,并且不受到其他人顶贴时间的限制

▲当周一至周四顶贴时,会发放2天空点与200游戏币且当你使用顶贴卡的前30分钟有其他人使用顶贴卡时,则只对你发放1天空点与600游戏币

### 顶贴步骤

* 1.打开[服务器宣传贴](https://www.mcbbs.net/thread-1057096-1-1.html)
* [2.在帖子底部右下角找到\_使用道具\_](https://www.mcbbs.net/home.php?mod=magic\&mid=mcbbs\_mcserver\_plus:serverBump\&idtype=tid\&id=1057096)
* [3.选择服务器/交易代理提升卡,点击购买后使用即可](https://www.mcbbs.net/home.php?mod=magic\&mid=mcbbs\_mcserver\_plus:serverBump\&idtype=tid\&id=1057096)

#### [_**点击此链接快速使用服务器/交易代理提升卡顶贴**_](https://www.mcbbs.net/home.php?mod=magic\&mid=mcbbs\_mcserver\_plus:serverBump\&idtype=tid\&id=1057096)_**(购买后再次点击本链接快速使用)**_

### **使用须知**

* 请勿同一时间连续使用服务器提升卡,<mark style="color:red;">**在90min内使用多张提升卡均会被判定只使用了一张(不会额外给予奖励)**</mark>
* 游戏币及天空点为插件自动发放,在菜单栏选择顶贴系统绑定<mark style="color:green;">**MCBBS ID**</mark>将会自动检测发放

附:如何找到自己的ID:访问 [**这个界面**](https://www.mcbbs.net/home.php?mod=spacecp) **,**图红框部分即为你的ID

![](.gitbook/assets/QQ截图20210703134259.png)
