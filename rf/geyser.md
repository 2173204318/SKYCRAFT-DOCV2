# 基岩版加入服务器

目前支持基岩版版本: 1.18.X&#x20;

IP:be.skycraft.cn&#x20;

<mark style="color:red;">**基岩版玩家专属端口:19132 (不懂进这个,最稳定,有基岩版专属菜单等快捷功能)**</mark>&#x20;

Java基岩玩家互通端口:19133 (不支持基岩版空格,[<mark style="color:green;">可参考此处更改代号方式</mark>](https://www.baidu.com/link?url=7rsFyHHqCdBA1lcsHX40AQ6bjIJLgIfN-X\_2LQ61IGSMfuKJWzovaBl5tUp-ASoJ\_fb9dd5Tk4YuiaIcUaH4kzFNWAUFf8NIYxGrt7f6CkCCcADSGNVAywOAyNC0Vhnz\&wd=\&eqid=a37231d00000d076000000056125e68a))

【基岩版玩家帮助请加入基岩版专属群】 群号: [285646110](https://jq.qq.com/?\_wv=1027\&k=7eF90zK1)

* 请使用最新国际版Minecraft.
* 如果要数据互通,请确保你的Xbox ID与Java版 ID相同且通过19133端口进入.
* 服务器提供了基岩版自动注册/登录功能,因此在使用19132端口时,请务必登录Xbox账号以保证数据安全(不登录Xbox账号无法登录服务器).
