# 服务器快捷指令帮助

<mark style="color:orange;">****</mark>[<mark style="color:orange;">**点我查看服务器常见问题**</mark>](wt.md)<mark style="color:orange;">****</mark>

服务器拥有一个便捷的菜单,打开指令是: `/菜单` 或 `/menu`,也可用 Shift+F 快速打开.

`岛屿菜单 /is`

`邀请玩家加入岛屿 /邀请 ID`

`岛屿指令帮助/is help`

`岛屿团队指令帮助(踢人/退出岛屿)/is team help`

`空岛任务系统 /c`&#x20;

`全服飞行 /fly`

`工作台 /wb`

`末影箱 /ec`

`在聊天栏展示物品 .i`

`快捷商店帮助 /qs`

`全服喊话/shout`

``
