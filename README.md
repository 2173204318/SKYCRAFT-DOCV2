---
description: 欢迎来到SkyCraft服务器官方文档 v2
cover: >-
  https://attachment.mcbbs.net/data/myattachment/forum/202007/23/164352i6845x6cqzxxwq4z.jpg
coverY: 0
---

# 首页

服务器IP: <mark style="color:purple;">**mc.skycraft.cn**</mark>

服务器QQ群: [<mark style="color:purple;">**599951039**</mark>](https://jq.skycraft.cn)

服务器官网: [<mark style="color:purple;">**skycraft.cn**</mark>**​**](https://skycraft.cn)

无偿赞助: [<mark style="color:purple;">**zz.skycraft.cn**</mark>**​**](https://zz.skycraft.cn)

## ****[<mark style="color:purple;">**查看服务器常见问题解答**</mark>](commands.md) <mark style="color:purple;">****</mark>** / **<mark style="color:purple;">****</mark> [<mark style="color:orange;">**服务器指令帮助**</mark>](commands.md)<mark style="color:orange;">****</mark>

如果您是新手,不知道怎么进入服务器,可访问[入服流程界面](rf/login.md)查看进入步骤.

服务器运行需要高昂的成本,如果您有能力,可访问[无偿赞助界面无偿赞助服务器](https://zz.skycraft.cn/),无偿赞助后您的ID可出现在[赞助名单](https://zz.skycraft.cn/zz)中.

## <mark style="color:red;"></mark>[<mark style="color:red;">服务器插件使用帮助</mark>](https://app.gitbook.com/s/QDPgBZ7bxCQO6geWHuys/)<mark style="color:red;"></mark>

****[**粘液科技**](https://docs.skycraft.cn/v/plugins/slimefun/slimefun)**:** [**无尽科技**](https://docs.skycraft.cn/v/plugins/slimefun/wjkj) **/** [**匠魂**](https://docs.skycraft.cn/v/plugins/slimefun/slimetinker) **/** [**网络**](https://docs.skycraft.cn/v/plugins/slimefun/networks) **/** [**简易工具**](https://docs.skycraft.cn/v/plugins/slimefun/simpleutils) **/** [**异域花园**](https://docs.skycraft.cn/v/plugins/slimefun/exoticgarden) **/** [**蓬松机器**](https://docs.skycraft.cn/v/plugins/slimefun/FluffyMachines) **/** [**魔法水晶编年史**](https://docs.skycraft.cn/v/plugins/slimefun/CrystamaeHistoria)****

****[**全球市场**](https://docs.skycraft.cn/v/plugins/quanqiu) **/** [**MCMMO**](https://docs.skycraft.cn/v/plugins/mcmmo) **/** [**负魔书**](https://docs.skycraft.cn/v/plugins/fu-mo-shu)****

## [<mark style="color:orange;">服务器材质包使用教程</mark>](https://docs.skycraft.cn/cz)

## [<mark style="color:green;">正版验证教程</mark>](https://docs.skycraft.cn/zb)

## [<mark style="color:blue;">顶贴奖励天空点教程</mark>](https://docs.skycraft.cn/dt)

如果你是服务器玩家,欢迎协助我们完善服务器文档,详情联系服主.
