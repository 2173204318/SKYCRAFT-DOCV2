# Table of contents

* [首页](README.md)
* [常见问题](wt.md)
* [服务器快捷指令帮助](commands.md)

## 新手教程 <a href="#rf" id="rf"></a>

* [加入服务器: 注册 / 登录](rf/login.md)
* [找回服务器登录密码](rf/cp.md)
* [基岩版加入服务器](rf/geyser.md)
  * [使用Pojav Launcher加入服务器](rf/geyser/pojav.md)

***

* [服务器材质包](cz.md)
* [反馈服务器BUG奖励天空点](bug.md)
* [正版验证](zb.md)
* [顶服务器宣传贴奖励(奖励天空点)](dt.md)
* [天空点(服务器贡献奖励机制)](sky.md)
* [服务器用户协议](https://qun.qq.com/qqweb/qunpro/share?\_wv=3&\_wwv=128\&inviteCode=144wj7\&contentID=GWsS\&businessType=2\&from=246610\&biz=ka\&shareSource=5)
* [豫ICP备2021034016号](https://beian.miit.gov.cn/)
