# 天空点(服务器贡献奖励机制)

{% hint style="info" %}
服务器不接受有偿赞助,天空点不能购买,只能通过顶贴/活动/反馈问题获得!
{% endhint %}

天空点商店:/pointshop

<mark style="color:yellow;">**每周末签到 / 使用签到币兑换**</mark>

****[<mark style="color:orange;">**天空点顶帖必得2点**</mark>](dt.md)<mark style="color:orange;">****</mark>

<mark style="color:purple;">****</mark>[<mark style="color:purple;">**反馈BUG奖励1点(定期统计)**</mark>](bug.md)<mark style="color:purple;">****</mark>

<mark style="color:red;">**举报作弊及其他行为奖励0-3点**</mark>
